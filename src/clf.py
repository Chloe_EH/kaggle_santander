import cPickle
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
import numpy as np

trainl = 13647309
testl = 929615
submissionl = 929615

X_columns = ['fecha_dato', 'ncodpers', 'ind_empleado',
             'pais_residencia',
             'sexo', 'age', 'antiguedad', 'indrel',
             'indrel_1mes', 'tiprel_1mes', 'indresi', 'indext',
             'conyuemp',  # 'canal_entrada',
             'indfall', 'tipodom',
             'cod_prov', 'ind_actividad_cliente', 'renta',
             'segmento']

Y_columns = ['ind_ahor_fin_ult1', 'ind_aval_fin_ult1',
               'ind_cco_fin_ult1', 'ind_cder_fin_ult1',
               'ind_cno_fin_ult1', 'ind_ctju_fin_ult1',
               'ind_ctma_fin_ult1', 'ind_ctop_fin_ult1',
               'ind_ctpp_fin_ult1', 'ind_deco_fin_ult1',
               'ind_deme_fin_ult1', 'ind_dela_fin_ult1',
               'ind_ecue_fin_ult1', 'ind_fond_fin_ult1',
               'ind_hip_fin_ult1', 'ind_plan_fin_ult1',
               'ind_pres_fin_ult1', 'ind_reca_fin_ult1',
               'ind_tjcr_fin_ult1', 'ind_valo_fin_ult1',
               'ind_viv_fin_ult1', 'ind_nomina_ult1',
               'ind_nom_pens_ult1', 'ind_recibo_ult1']

columns = X_columns + Y_columns


def downsize_data(df, trainset=True):
    countries = ['LV', 'BE', 'BG', 'BA', 'BM', 'BO', 'JP', 'JM', 'BR',
                 'CR', 'BY', 'BZ', 'RU', 'RS', 'RO', 'GW', 'GT', 'GR',
                 'GQ', 'GE', 'GB', 'GA', 'GN', 'GM', 'GI', 'GH', 'OM',
                 'HR', 'HU', 'HK', 'HN', 'AD', 'PR', 'PT', 'PY', 'PA',
                 'PE', 'PK', 'PH', 'PL', 'EE', 'EG', 'ZA', 'EC', 'IT',
                 'AO', 'ET', 'ZW', 'ES', 'MD', 'MA', 'MM', 'ML', 'MK',
                 'MT', 'MR', 'UA', 'MX', 'AT', 'FR', 'FI', 'NI', 'NL',
                 'NO', 'NG', 'NZ', 'CI', 'CH', 'CO', 'CN', 'CM', 'CL',
                 'CA', 'CG', 'CF', 'CD', 'CZ', 'UY', 'CU', 'KE', 'KH',
                 'SV', 'SK', 'KR', 'KW', 'SN', 'SL', 'KZ', 'SA', 'SG',
                 'SE', 'DO', 'DJ', 'DK', 'DE', 'DZ', 'US', 'LB', 'TW',
                 'TR', 'TN', 'LT', 'LU', 'TH', 'TG', 'LY', 'AE', 'VE',
                 'IS', 'AL', 'VN', 'AR', 'AU', 'IL', 'IN', 'IE', 'QA',
                 'MZ']

    values = range(1, len(countries) + 1)
    dict_countries = dict(zip(countries, values))

    di = {'conyuemp': {'0': 0, 'N': 0, 'S': 1},
          'sexo': {'H': 1, 'V': 2},
          'pais_residencia': dict_countries,
          'indresi': {'N': 1, 'S': 2},
          'segmento': {'01 - TOP': 1, '01 - VIP': 1, '02 - PARTICULARES': 2, '03 - UNIVERSITARIO': 3},
          'indext': {'N': 1, 'S': 2},
          'ind_empleado': {'A': 1, 'B': 2, 'F': 3, 'N': 4, 'P': 5, 'S': -99},
          'tiprel_1mes': {'A': 1, 'I': 2, 'P': 3, 'R': 4, 'N': -99},
          'indfall': {'N': 1, 'S': 2},
          'indrel_1mes': {'1': 1, '1.0': 1, '2': 2, '2.0': 2,
                          '3': 3, '3.0': 3, '4': 4, '4.0': 4,
                          'P': 5}
          }

    df.replace(to_replace=di, inplace=True)
    df['fecha_dato'] = df['fecha_dato'].apply(lambda x: (100 * x.year) + x.month).astype(np.int8)
    df['age'] = df['age'].apply(lambda x: 0 if x.find('NA') != -1 else x).astype(np.int8)
    df['antiguedad'] = df['antiguedad'].apply(lambda x: 0 if x.find('NA') != -1 else x).astype(np.int16)

    df.dropna(thresh=10, inplace=True, subset=[X_columns])
    df.fillna(-99, inplace=True)

    col_int8 = ['fecha_dato', 'ind_empleado', 'pais_residencia',
                 'sexo', 'age', 'indrel', 'indrel_1mes',
                 'tiprel_1mes', 'indresi', 'indext',
                 'conyuemp', 'indfall', 'tipodom', 'cod_prov',
                 'ind_actividad_cliente', 'segmento']

    if trainset:
        col_int8.append(Y_columns)

    for key in col_int8:
        df[key] = df[key].astype(np.int8)

    return df


def load_data(file_path, trainset=True):
    chunk_size = 500000

    if trainset:
        cols = columns
    else:
        cols = X_columns

    reader = pd.read_csv(file_path, iterator=True, chunksize=chunk_size,
                        parse_dates=['fecha_dato'],
                        dtype={
                            'conyuemp': str,
                            'age': str,
                            'antiguedad': str,
                            'indrel_1mes': str,
                            'pais_residencia': str},
                        usecols=cols)

    # df = downsize_data(reader.get_chunk(chunk_size))
    df = pd.concat(downsize_data(chunk, trainset) for chunk in reader)

    # print df.info(memory_usage=True)
    # with open('data.pkl', 'wb') as fid:
    #     cPickle.dump(df, fid)

    return df


def transform_scale(X):
    scaler = cPickle.load(open('stdscaler.pkl', 'rb'))
    return scaler.transform(X)


def train(df):    
    clf = RandomForestClassifier(n_estimators=100, n_jobs=-1, random_state=123, max_features=11)
    X = df[X_columns]
    Y = df[Y_columns]

    print '....Start training....'
    clf.fit(X, Y)
    print '....finish training....'
    importances = clf.feature_importances_
    indices = np.argsort(importances)[::-1]

    for f in range(len(columns)):
        print("%d. %s (%f)" % (f, columns[indices[f]], importances[indices[f]]))

    with open('clf.pkl', 'wb') as fid:
        cPickle.dump(clf, fid)


def test(df):
    clf = cPickle.load(open('clf.pkl', 'rb'))
    result = clf.predict_proba(df)
    out = pd.DataFrame(result, columns= Y_columns)
    out['ncodpers'] = df['ncodpers']
    del df

    out.to_csv('submission.csv')

if __name__ == '__main__':
    # df = load_data('../train_ver2.csv')
    df = cPickle.load(open('data.pkl', 'rb'))
    print df.info(memory_usage =True)
    train(df)
    del df

    df = load_data('../test_ver2.csv', trainset=False)
    test(df)
